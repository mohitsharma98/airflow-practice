from airflow import DAG
from airflow.operators.dummy import DummyOperator
from datetime import datetime
from random import randint

with DAG("simpleDag", start_date=datetime(2022,3,6), schedule_interval="@daily", catchup=False) as dag:

    start = DummyOperator(
        task_id="start",
    )

    end = DummyOperator(
        task_id="end",
    )

    start >> end